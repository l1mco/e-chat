from django.urls import path, include
from rest_framework.documentation import include_docs_urls
from rest_framework.permissions import AllowAny

urlpatterns = [
    path('users/', include('apps.users.urls')),
    path('chat/', include('apps.chat.urls')),
    path('docs/', include_docs_urls(
        title='E-Chat API', description='API docs for E-Chat', public=True,
        permission_classes=(AllowAny,))
         )
]

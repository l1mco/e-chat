from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from apps.users.managers import UserManager
from utils.upload_avatar_file import upload_instance
from utils.user_phone_number_regex import phone_regex


class User(AbstractUser):
    username = None
    first_name = models.CharField(
        max_length=127, verbose_name='Имя', help_text='Имя'
    )
    second_name = models.CharField(
        max_length=127, verbose_name='Фамилия', help_text='Фамилия'
    )
    middle_name = models.CharField(
        max_length=127, blank=True, null=True, verbose_name='Отчество',
        help_text='Отчество'
    )
    phone = models.CharField(
        max_length=50, validators=[phone_regex], unique=True,
        verbose_name='Номер', help_text='Телефона, формат: +996000123123'
    )
    avatar = models.ImageField(
        upload_to=upload_instance, null=True, blank=True, verbose_name='Аватар',
        help_text='Аватар'
    )

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['first_name', 'second_name']

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.get_full_name

    @property
    def get_full_name(self):
        full_name = ''

        if self.last_name:
            full_name += self.last_name
        if self.first_name:
            full_name += f' {self.first_name}'
        if self.middle_name:
            full_name += f' {self.middle_name}'

        return full_name


@receiver(post_save, sender=User)
def create_auth_token(instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

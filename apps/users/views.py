from rest_framework.generics import CreateAPIView, RetrieveAPIView, ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated

from apps.users.models import User
from apps.users.serializers import UserListSerializer, UserRegisterSerializer


class UserRegisterAPIView(CreateAPIView):
    """
    Endpoint for user register
    """
    model = User
    permission_classes = (AllowAny,)
    serializer_class = UserRegisterSerializer


class UserDetailAPIView(RetrieveAPIView):
    """
    Endpoint for get user detail
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRegisterSerializer
    queryset = User.objects.all()


class UserListAPIView(ListAPIView):
    """
    Endpoint for get users list
    """
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = UserListSerializer

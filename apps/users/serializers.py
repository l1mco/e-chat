from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.users.models import User


class UserRegisterSerializer(serializers.ModelSerializer):
    """ Serializer for user register """

    token = serializers.CharField(source='auth_token.key', read_only=True)
    phone = serializers.CharField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'id', 'first_name', 'second_name', 'middle_name', 'phone',
            'avatar', 'token', 'password', 'password2'
        )
        extra_kwargs = {
            'first_name': {'required': True},
            'phone': {'required': True},
            'second_name': {'required': True},
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."}
            )

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            first_name=validated_data['first_name'],
            middle_name=validated_data.get('middle_name', None),
            phone=validated_data['phone'],
            avatar=validated_data.get('avatar', None),
            second_name=validated_data['second_name'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class UserListSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')

    class Meta:
        model = User
        fields = ('id', 'full_name', 'avatar',)

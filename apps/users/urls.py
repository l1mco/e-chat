from django.urls import path, include
from rest_framework.authtoken import views

from apps.users.views import (
    UserRegisterAPIView, UserDetailAPIView, UserListAPIView
)

urlpatterns = [
    path('', UserListAPIView.as_view(), name='user_detail'),
    path('<int:pk>/', UserDetailAPIView.as_view(), name='user_detail'),
    path('register/', UserRegisterAPIView.as_view(), name='user_register'),
    path('token/', views.obtain_auth_token)
]

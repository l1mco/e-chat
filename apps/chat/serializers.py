from rest_framework import serializers

from apps.chat.models import Chat, Message
from apps.users.models import User


class ChatListUserSerializer(serializers.ModelSerializer):
    full_name = serializers.CharField(source='get_full_name')

    class Meta:
        model = User
        fields = ('id', 'full_name', 'avatar')


class MessageSerializer(serializers.ModelSerializer):
    author = ChatListUserSerializer()

    class Meta:
        model = Message
        fields = ('id', 'author', 'text')


class ChatSerializer(serializers.ModelSerializer):
    last_message = MessageSerializer(source='get_last_message')

    class Meta:
        model = Chat
        fields = ('id', 'creator', 'receiver', 'last_message')
        read_only_fields = ('id', 'last_message')


class ChatCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Chat
        fields = ('id', 'receiver', 'creator')
        read_only_fields = ('id', 'creator')


class MessageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'author', 'attached_file')
        read_only_fields = ('id',)


class FileUploadSerializer(serializers.Serializer):
    file = serializers.FileField()
    message = serializers.IntegerField()

import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from .models import Message
from .services import ChatService


chat_service = ChatService


class SyncChatConsumer(WebsocketConsumer):

    def all_messages(self, data):
        messages = (
            Message.objects
            .filter(chat=self.chat)
            .order_by('-send_time')
        )
        content = {
            'messages': self.messages_to_json(messages)
        }
        self.send_message(content)

    def fetch_messages(self, data):

        messages = self.chat.get_last_10_messages
        content = {
            'messages': self.messages_to_json(messages)
        }
        self.send_message(content)

    def new_message(self, data):
        current_chat = self.chat
        message = Message.objects.create(
            text=data['text'],
            chat=current_chat,
            author=self.user
        )
        content = {
            'message': self.message_to_json(message)
        }

        return self.send_chat_message(content)

    commands = {
        'all_messages': all_messages,
        'fetch_messages': fetch_messages,
        'new_message': new_message
    }

    def messages_to_json(self, messages):
        result = []
        for message in messages:
            result.append(self.message_to_json(message))
        return result

    def message_to_json(self, message):
        message_data = dict(
            id=message.id,
            author=message.author.id,
            text=message.text,
            send_time=message.format_send_time,
        )

        if message.attached_file:
            message_data['attached_file'] = message.get_attached_file_url

        return message_data

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['pk']
        self.room_group_name = 'chat_%s' % self.room_name
        chat = chat_service.get_chat_by_id(self.room_name)

        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        if not self.scope['user'] or self.scope['user'].is_anonymous:
            self.error_response('User not found')

        self.user = self.scope['user']

        if not chat:
            self.error_response('Chat not found')

        self.chat = chat

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):

        data = json.loads(text_data)
        command = data['command']

        if command not in self.commands or not command:
            self.error_response(f'Command {command} not valid')

        self.commands[command](self, data)

    def send_chat_message(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def send_message(self, message):
        self.send(text_data=json.dumps(message))

    def chat_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))

    def error_response(self, message):

        self.send(text_data=json.dumps(dict(
            message=message
        )))
        self.close()

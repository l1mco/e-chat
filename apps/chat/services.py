from apps.chat.models import Chat
from apps.users.models import User


class ChatService:

    @classmethod
    def get_user_by_id(cls, user_id):
        return User.objects.filter(id=user_id).first()

    @classmethod
    def get_chat_by_id(cls, chat_id):
        return Chat.objects.filter(id=chat_id).first()

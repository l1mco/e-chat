from channels.auth import AuthMiddlewareStack
from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from django.contrib.auth.models import AnonymousUser

from apps.users.models import User


@database_sync_to_async
def get_user(token_key):
    user = User.objects.filter(auth_token__key=token_key).first()
    if not user:
        return AnonymousUser()

    return user


class TokenAuthMiddleware(BaseMiddleware):

    def __init__(self, inner):
        super().__init__(inner)
        self.inner = inner

    async def __call__(self, scope, receive, send):

        headers = dict(scope['headers'])

        if b'authorization' in headers:
            token_name, token_key = headers[b'authorization'].decode().split()
            if token_key:
                scope['user'] = await get_user(token_key)

        return await super().__call__(scope, receive, send)


TokenAuthMiddlewareStack = (
    lambda inner: TokenAuthMiddleware(AuthMiddlewareStack(inner))
)

from django.db.models import Q
from django.http import JsonResponse
from rest_framework import status
from rest_framework.generics import (
    ListCreateAPIView, RetrieveAPIView, GenericAPIView, CreateAPIView, ListAPIView
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.chat.models import Chat, Message
from apps.chat.serializers import (
    ChatSerializer, FileUploadSerializer, MessageFileSerializer,
    ChatCreateSerializer
)


class ChatListAPIView(ListAPIView):
    """
    Endpoint for get chat list
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ChatSerializer

    def get_queryset(self):
        user = self.request.user
        return Chat.objects.filter(Q(creator=user) | Q(receiver=user)).distinct()


class ChatCreateAPIView(GenericAPIView):
    """
    Endpoint for create chat
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ChatCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        receiver_user = serializer.validated_data['receiver']
        creator_user = self.request.user

        existed_chat = (
            Chat.objects.filter(
                (Q(creator=creator_user) | Q(receiver=creator_user)),
                (Q(creator=receiver_user) | Q(receiver=receiver_user)),
            )
            .distinct()
            .first()
        )
        if not existed_chat:
            chat = Chat.objects.create(
                creator=self.request.user,
                receiver=serializer.validated_data['receiver']
            )

            return Response(
                ChatCreateSerializer(chat).data,
                status=status.HTTP_201_CREATED
            )

        return Response(
            ChatCreateSerializer(existed_chat).data,
            status=status.HTTP_200_OK
        )


class FileUploadAPIView(GenericAPIView):
    serializer_class = FileUploadSerializer

    def post(self, request, *args, **kwargs):
        serializer_data = self.serializer_class(data=request.data)
        serializer_data.is_valid(raise_exception=True)
        message_id = serializer_data.validated_data['message']
        file = serializer_data.validated_data['file']

        message = Message.objects.filter(id=message_id).first()
        message.attached_file = file
        message.save(update_fields=['attached_file'])

        message_serializer = MessageFileSerializer(message, context={
            'request': self.request
        })

        return JsonResponse(
            dict(
                message=message_serializer.data,
            ),
            status=200
        )

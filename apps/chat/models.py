from django.conf import settings
from django.db import models
from django.utils import timezone

from apps.users.models import User
from utils.upload_message_attaches import upload_message_attach


tz = timezone.get_default_timezone()


class Chat(models.Model):
    creator = models.ForeignKey(
        User, related_name='chat_creator', on_delete=models.SET_NULL, null=True,
        verbose_name='Создатель'
    )
    receiver = models.ForeignKey(
        User, related_name='chat_receiver', on_delete=models.SET_NULL,
        null=True, verbose_name='Получатель'
    )
    created_at = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return f'{self.creator} - {self.receiver}'

    @property
    def get_last_10_messages(self):
        return self.chat_messages.all().order_by('-send_time')[:10]

    @property
    def get_last_message(self):
        return self.chat_messages.order_by('-send_time').last()


class Message(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='author_messages',
        verbose_name='Автор', null=True, blank=True
    )
    chat = models.ForeignKey(
        Chat, on_delete=models.SET_NULL, null=True,
        related_name='chat_messages', verbose_name='Чат'
    )
    text = models.TextField(blank=True, null=True, verbose_name='Текст')
    attached_file = models.FileField(
        upload_to=upload_message_attach, blank=True, null=True,
        verbose_name='Прикрепленный файл'
    )
    send_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.id}'

    @property
    def format_send_time(self):
        return self.send_time.astimezone(tz).strftime('%d.%m.%Y %H:%M:%S')

    @property
    def get_attached_file_url(self):
        url = (
            f'{settings.SITE_PROTOCOL}://'
            f'{settings.SITE_DOMAIN}'
            f'{self.attached_file.url}'
        )
        return url

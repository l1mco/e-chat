from django.urls import path

from apps.chat.views import (
    ChatListAPIView, ChatCreateAPIView, FileUploadAPIView,
)

urlpatterns = [
    path('', ChatListAPIView.as_view(), name='chat_list'),
    path('create/', ChatCreateAPIView.as_view(), name='chat_create'),
    path('file/', FileUploadAPIView.as_view(), name='file_upload'),
]

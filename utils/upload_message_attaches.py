import uuid


def upload_message_attach(instance, filename):
    ext = filename.split('.')[-1]
    filename = f'{uuid.uuid4()}.{ext}'

    return 'uploads/%s/%s/%s' % (
        instance.chat.id,
        instance.__class__.__name__,
        filename,
    )

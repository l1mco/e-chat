FROM python:3.8-alpine

LABEL Description="E-chat"
MAINTAINER l1mco <isaevsmail@gmail.com>

ARG BACKEND_WEB_PORT

WORKDIR /opt/app
COPY . .

RUN apk update && \
    apk add --no-cache \
    --virtual .build-deps \
    gcc postgresql-dev musl-dev \
    python3-dev py3-pillow libxslt-dev \
    libc-dev geos-dev geos openssl-dev \
    cargo libffi-dev gettext jpeg-dev\
    zlib-dev

RUN pip install -r requirements/prod.txt


RUN chmod +x /opt/app/docker-entrypoint.sh

EXPOSE $BACKEND_WEB_PORT

ENTRYPOINT ["/opt/app/docker-entrypoint.sh"]

import os
from urllib.parse import urljoin

import dj_database_url

DEBUG = os.environ.get('DJANGO_DEBUG', False)

SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY')

ALLOWED_HOSTS = ['*']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    },
}

DATABASES = {
    'default': dj_database_url.parse(
        os.environ['BACKEND_DB_URL']
    )
}


SITE_PROTOCOL = os.environ['SITE_PROTOCOL']
SITE_DOMAIN = os.environ['SITE_DOMAIN']


DEFAULT_FILE_STORAGE = 'minio_storage.storage.MinioMediaStorage'
STATICFILES_STORAGE = 'minio_storage.storage.MinioStaticStorage'
MINIO_STORAGE_ACCESS_KEY = os.environ['MINIO_ACCESS_KEY']
MINIO_STORAGE_SECRET_KEY = os.environ['MINIO_SECRET_KEY']
MINIO_STORAGE_ENDPOINT = os.environ['MINIO_INTERNAL_HOST']
DEFAULT_STORAGE_ENDPOINT = os.environ['MINIO_ENDPOINT_HOST']
MINIO_STORAGE_MEDIA_BUCKET_NAME = 'backend-media'
MINIO_STORAGE_AUTO_CREATE_MEDIA_BUCKET = True
MINIO_STORAGE_STATIC_BUCKET_NAME = 'backend-static'
MINIO_STORAGE_AUTO_CREATE_STATIC_BUCKET = True
MINIO_STORAGE_STATIC_URL = urljoin(
    DEFAULT_STORAGE_ENDPOINT, MINIO_STORAGE_STATIC_BUCKET_NAME
)
MINIO_STORAGE_MEDIA_URL = urljoin(
    DEFAULT_STORAGE_ENDPOINT, MINIO_STORAGE_MEDIA_BUCKET_NAME
)
MINIO_STORAGE_MEDIA_USE_PRESIGNED = True
MINIO_STORAGE_USE_HTTPS = False

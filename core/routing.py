from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path

from apps.chat.consumers import SyncChatConsumer
from apps.chat.middlewares import TokenAuthMiddlewareStack

application = ProtocolTypeRouter({
    "websocket": TokenAuthMiddlewareStack(
        URLRouter([
            path('ws/chat/<int:pk>/', SyncChatConsumer.as_asgi()),
        ])
    ),
})
